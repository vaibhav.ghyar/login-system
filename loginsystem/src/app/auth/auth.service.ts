import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  getAuthStatus(){
    if(localStorage.getItem("Auth")!=null){
      return true;
    }
      return false;
  }

}
